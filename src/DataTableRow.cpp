#include "DataTableRow.h"
#include "DataTableColumnBase.h"
#include "DataTable.h"

DataTableRow::~DataTableRow()
{
    //dtor
}

DataTableRow::DataTableRow(DataTable* table)
    : _table(table)
{
}

DataTableColumnBase* DataTableRow::GetColumn(std::string name) const
{
    return _table->GetColumn(name);
}

void DataTableRow::SetRowNumber(int number)
{
    _rowNumber = number;
}

/**
 * At the moment, to preserve order while cutting down on access times
 * DataTable's DataTableColumnCollection uses both a map<key, value>
 * and a vector<key>. ColumnCollection's iterator iterates through the vector,
 * thus returning mere std::strings---the column names.
 * We have to find the actual columns ourselves, but they're in an unordered_map so it should be fast.
 */
std::string DataTableRow::ToString()
{
    std::string rowString = "";

    std::string name;
    std::string fieldString;
    DataTableColumnBase* Column;

    for(auto it = _table->ColumnBegin(); it != _table->ColumnEnd(); ++it)
    {
        name = *it;
        Column = GetColumn(name);
        fieldString = Column->GetAsString(_rowNumber);
        rowString = rowString + fieldString + "\t";
    }

    return rowString;
}
