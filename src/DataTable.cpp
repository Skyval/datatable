#include "DataTable.h"

#include <string>
#include <iostream>

void DataTable::AddColumn(DataTableColumnBase* Column)
{
    _columnCollection.AddColumn( Column );
}

void DataTable::PrintColumnNames()
{
    _columnCollection.PrintColumnNames();
}

DataTableRow* DataTable::NewRow()
{
    DataTableRow* NewRow = new DataTableRow(this);
    _rowCollection.AddRow(NewRow);

    if(_columnCollection.GetCapacity() != _rowCollection.size())
    {
        _columnCollection.ChangeCapacity(_rowCollection.size());
    }

    return NewRow;
}

DataTableColumnBase* DataTable::GetColumn(std::string name)
{
    return _columnCollection.GetColumn(name);
}

void DataTable::PrintTable()
{
    std::string names = _columnCollection.GetNames();
    std::cout << names << std::endl;

    std::string test;

    for(DataTableRowCollection::iterator index = _rowCollection.begin(); index != _rowCollection.end(); ++index)
    {
        test = (*index)->ToString();

        std::cout << test << std::endl;
    }
}


