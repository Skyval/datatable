#include "DataTableRowCollection.h"

DataTableRowCollection::~DataTableRowCollection()
{
    for(DataTableRow* Row : _rows)
    {
        delete Row;
    }
}

void DataTableRowCollection::AddRow(DataTableRow* Row)
{
    _rows.push_back(Row);
    Row->SetRowNumber(_rows.size());
}

int DataTableRowCollection::size()
{
    return _rows.size();
}
