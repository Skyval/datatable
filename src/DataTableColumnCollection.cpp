#include "DataTableColumnCollection.h"

#include <iostream>
#include <string>

DataTableColumnCollection::~DataTableColumnCollection()
{
    for( const auto& kv : _columns)
    {
        delete kv.second;

        //I guess if I'm destroying the map, I don't really need to remove individual entries?
        // _columns.erase(kv);// UNTESTED
        //i = _columns.erase(i);// LEGACY (old iteration)
    }
}


void DataTableColumnCollection::AddColumn(DataTableColumnBase* Column)
{
    std::string name = Column->GetName();

    auto it = _columns.find(name);

    if( it != _columns.end())
    {
        std::cout << "Collision detected." << std::endl;
    }
    else
    {
        _KeyOrder.push_back(name);
        _columns[name] = Column;
    }
}

void DataTableColumnCollection::PrintColumnNames()
{
    for( const auto& name : _KeyOrder)
    {
        std::cout << name << std::endl;
    }
}

DataTableColumnBase* DataTableColumnCollection::GetColumn(std::string name)
{
    auto it = _columns.find(name);

    if(it != _columns.end())
    {
        return (*it).second;
    }

    return nullptr;
}

int DataTableColumnCollection::size()
{
    return _columns.size();
}

int DataTableColumnCollection::GetCapacity()
{
    return _capacity;
}

void DataTableColumnCollection::ChangeCapacity(int newCapacity)
{
    _capacity = newCapacity;

    for( const auto& kv : _columns)
    {
        kv.second->resize(_capacity);
    }
}


std::string DataTableColumnCollection::GetNames()
{
    std::string result = "";

    for( const auto& name : _KeyOrder)
    {
        result = result + name + "\t";
    }

    return result;
}




