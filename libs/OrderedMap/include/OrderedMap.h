#ifndef ORDEREDMAP_H
#define ORDEREDMAP_H

#include <unordered_map>
#include <vector>

template <typename KeyType, typename ValueType>
class OrderedMap
{

    // We should never change elements in the order vector. The order vector also only contains, keys, and we can't change keys in the map either, so there shouldn't ever be a "desync".
    // Actually, there could still be a problem if pair<const key, value> is changed directly, like if trying to reorder the map?
    // Could probably be solved with an assignment operator overload in OrderedMap_Iterator.
    using vector_iterator = typename std::vector<KeyType>::const_iterator;
    using under_map = typename std::unordered_map<KeyType, ValueType>;


public:

    template <typename T>
    class OrderedMap_Iterator// : public std::iterator<std::random_access_iterator_tag, ValueType>
    {
        vector_iterator _Current;
        const under_map& _Map;

    public:

        OrderedMap_Iterator(vector_iterator it, const under_map& Map) : _Current(it), _Map(Map) {}

        const OrderedMap_Iterator& operator++() { ++_Current; return *this; }
        OrderedMap_Iterator operator++(int) { OrderedMap_Iterator result(*this); ++(*this); return result; }

        T& operator*() const
        {
            // I'm pretty confident under_map::find doesn't actually modify the map, even without const_iterator
            typename under_map::iterator it = const_cast<under_map*>(&_Map)->find(*_Current);
            return *it;
        }

        T* operator->() const
        {
            // Reuse OrderedMap_Iterator::operator*().
            // Written this way for clarity.
            // Could could have been written as:  '&(*(*this))' or even '&operator*()'.
            return &this->operator*();
        }

        bool operator==(const OrderedMap_Iterator& rhs) const { return _Current == (rhs._Current); }
        bool operator!=(const OrderedMap_Iterator& rhs) const { return _Current != (rhs._Current); }
    };

    using iterator = OrderedMap_Iterator<std::pair<const KeyType, ValueType>>;
    using const_iterator = OrderedMap_Iterator<const std::pair<const KeyType, ValueType>>;

    OrderedMap() = default;
    virtual ~OrderedMap() = default;

    iterator begin()
    {
        iterator it(_Vector.cbegin(), _Map);
        return it;
    }

    iterator end()
    {
        iterator it(_Vector.cend(), _Map);
        return it;
    }

    const_iterator cbegin() const
    {
        const_iterator it(_Vector.cbegin(), _Map);
        return it;
    }

    const_iterator cend() const
    {
        const_iterator it(_Vector.cend(), _Map);
        return it;
    }

    void add(KeyType Key, ValueType Value)
    {
        _Map[Key] = Value; _Vector.push_back(Key);
    }

private:
    under_map _Map;
    std::vector<KeyType> _Vector;
};

#endif // ORDEREDMAP_H
