#include <iostream>
#include <string>

#include "DataTable.h"
#include "DataType.h"
#include "OrderedMap.h"

using std::cout;
using std::endl;
using std::cin;


int main()
{
    OrderedMap<std::string, int> Map;

    Map.add("one", 1);
    Map.add("two", 2);

    /*
    DataTable Table;

    cout << "Creating a table." << endl;

    EDataType type;
    bool done = false;
    std::string input;

    while(!done)
    {
        cout << "Column type: " << endl;
        cout << "1 - int" << endl;
        cout << "3 - string" << endl;
        std::getline(cin, input);
        type = static_cast<EDataType>(std::stoi(input));

        std::string name = "";
        cout << "Column name: " << endl;
        std::getline(cin, input);

        switch(type)
        {
        case EDataType::INT:
            Table.NewColumn<int>(input);
            break;
        case EDataType::STRING:
            Table.NewColumn<std::string>(input);
            break;
        default:
            break;
        }

        cout << "Enter another column?" << endl;
        cout << "0 - yes" << endl;
        cout << "1 - no" << endl;
        std::getline(cin, input);
        done = static_cast<bool>(std::stoi(input));
    }

    done = false;

    int rowNumber = 1;
    while(!done)
    {
        cout << "Enter data for row" << endl;
        Table.NewRow();

        for(auto i = Table.ColumnBegin(); i != Table.ColumnEnd(); ++i)
        {
            std::getline(cin, input);
            (*i)->Set<std::string>(rowNumber, input);
        }
        ++rowNumber;

        cout << "Enter another record?" << endl;
        cout << "0 - yes" << endl;
        cout << "1 - no" << endl;
        std::getline(cin, input);
        done = static_cast<bool>(std::stoi(input));
    }

    Table.PrintTable();
    */

    DataTable People;
    People.NewColumn<std::string>("First Name");
    People.NewColumn<std::string>("Last Name");
    People.NewColumn<std::string>("Country");
    People.NewColumn<std::string>("City");
    People.NewColumn<std::string>("Phone Number");
    People.NewColumn<std::string>("Birth Date");
    People.NewColumn<int>("Age");

    DataTableRow* BakerCRec = People.NewRow();
    BakerCRec->Set<std::string>("First Name", "Cody");
    BakerCRec->Set<std::string>("Last Name", "Baker");
    BakerCRec->Set<std::string>("Country", "United States");
    BakerCRec->Set<std::string>("City", "Baytown");
    BakerCRec->Set<std::string>("Phone Number", "281-974-7071");
    BakerCRec->Set<std::string>("Birth Date", "1992-12-09");
    BakerCRec->Set<int>("Age", 23);

    DataTableRow* BakerKRec = People.NewRow();
    BakerKRec->Set<std::string>("First Name", "Kaci");
    BakerKRec->Set<std::string>("Last Name", "Baker");
    BakerKRec->Set<std::string>("Country", "United States");
    BakerKRec->Set<std::string>("City", "Baytown");
    BakerKRec->Set<std::string>("Phone Number", "555-555-5555");
    BakerKRec->Set<std::string>("Birth Date", "1998-11-23");
    BakerKRec->Set<int>("Age", 17);

    People.PrintTable();

    return 0;
}
