#ifndef DATASTORAGE_H
#define DATASTORAGE_H

#include "DataType.h"

class DataStorage
{
    public:
        DataStorage();
        virtual ~DataStorage();

        static DataStorage CreateStorage(EDataType DataType);

    protected:

    private:
};

#endif // DATASTORAGE_H
