#ifndef DATATABLECOLUMN_H
#define DATATABLECOLUMN_H

#include <string>
#include <vector>

#include <sstream>

#include "DataTableColumnBase.h"


template<typename T>
class DataTableColumn : public DataTableColumnBase
{
    public:

        virtual ~DataTableColumn() = default;

        DataTableColumn(std::string name)
            : DataTableColumnBase(name)
        {}

        DataTableColumn(const DataTableColumn<T>& that) = delete;


        void resize(int newSize)
        {
            _storage.resize(newSize);
        }

        virtual std::string GetAsString(int RowNumber) override
        {
            std::ostringstream oss;
            oss << (*this)[RowNumber];//_storage[RowNumber-1];
            return oss.str();
        }

        void Set(int RowNumber, T Input)
        {
            _storage[RowNumber-1] = Input;
        }

        T& operator[](const int& RowNumber)
        {
            return _storage[RowNumber-1];
        }

    private:
        std::vector<T> _storage;

};

#endif // DATATABLECOLUMN_H
