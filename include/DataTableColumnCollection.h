#ifndef DATATABLECOLUMNCOLLECTION_H
#define DATATABLECOLUMNCOLLECTION_H

#include <map>
#include <string>

#include "DataTableColumn.h"

class DataTableColumnCollection
{
    public:

        //typedef std::map<std::string, DataTableColumnBase*>::iterator iterator;
        //typedef std::map<std::string, DataTableColumnBase*>::const_iterator const_iterator;
        typedef std::vector<std::string>::iterator iterator;
        typedef std::vector<std::string>::const_iterator const_iterator;

        iterator begin() { return _KeyOrder.begin(); }
        const_iterator begin() const { return _KeyOrder.begin(); }

        iterator end() { return _KeyOrder.end(); }
        const_iterator end() const { return _KeyOrder.end(); }

        ~DataTableColumnCollection();

        void AddColumn(DataTableColumnBase* Column);
        void PrintColumnNames();
        //void Expand();
        int size(); // Number of columns in collection
        int GetCapacity(); // Number of entries per column
        //void resize(int newSize); // changes number of columns in collection
        void ChangeCapacity(int newCapacity); // Resizes each column within the collection
        DataTableColumnBase* GetColumn(std::string name);
        std::string GetNames();

    protected:

    private:

        std::map<std::string, DataTableColumnBase*> _columns;
        std::vector<std::string> _KeyOrder;

        int _capacity = 0;
};

#endif // DATATABLECOLUMNCOLLECTION_H
