#ifndef RESULT_H
#define RESULT_H

#include <string>

template <typename T>
class Result
{
    public:

        /** Default constructor. Defaults to invalid. */
        Result() : _errorString(""), _valid(false) { }

        /** Conversion constructor. */
        Result(T content) : _result(content), _valid(true) {}



        ~Result() {}

        /** Copy constructor */
        Result(const Result<T>& other)
        {
            if(_valid)
            {
                this->_result = other._result;
            }
            else
            {
                this->_errorString = other._errorString;
            }
        }

        /** Move constructor */
        Result( Result<T>&& other)
        {
            *this = std::move(other);
        }

        /** Gets the value, may not be useful if invalid. */
        T get()
        {
            return this->_result;
        }

        /** Gets the error, may not be useful if valid. */
        std::string getError()
        {
            return _errorString;
        }

        /** Copy assignment */
        Result<T>& operator=(const Result<T>& rhs)
        {
            if(this != &rhs)
            {
                if(_valid)
                {
                    this->_result = rhs._result;
                }
                else
                {
                    this->_errorString = rhs._errorString;
                }

            }

            return *this;
        }

        /** Move assignment */
        Result<T>& operator=(Result<T>&& rhs)
        {
            if(this != &rhs)
            {
                *this = std::move(rhs);
            }

            return *this;
        }

        /** T assignment */
        Result<T>& operator=(const T& rhs)
        {
            this->_valid = true;
            this->_result = rhs;
            return *this;
        }

        /**
         * Creates a Result<T> which contains an error instead of normal data.
         * Needed a non-constructor since it's possible a Result<T>'s normal data could be another "error".
         */
        static Result<T> fromError(std::string errorString)
        {
            Result<T> newResult;
            newResult._errorString = errorString;

            return newResult;
        }

        /** Checks if Result<T> has a value or not. If not, it has an error instead. */
        bool valid()
        {
            return _valid;
        }

    protected:

    private:
        union
        {
            T _result;
            std::string _errorString;
        };

        bool _valid;
};

#endif // RESULT_H
