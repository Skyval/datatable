#ifndef DATATABLECOLUMNBASE_H
#define DATATABLECOLUMNBASE_H

#include "Result.h"

template<typename U>
class DataTableColumn;

class DataTableColumnBase
{
    public:
        DataTableColumnBase();
        virtual ~DataTableColumnBase();

        DataTableColumnBase(std::string name);

        std::string GetName();

        virtual void resize(int newSize) = 0;

        virtual std::string GetAsString(int RowNumber) = 0;

        /**
         * Gets the data at the specified row.
         *
         * @param RowNumber     The row to get the data for.
         * @return Result<T>    Holds value if successful, or an error sting if failure. Can be tested with .valid()
         */
        template <typename T>
        Result<T> Get(const int& RowNumber)
        {
            // Make sure the column is the right type
            DataTableColumn<T>* Column = dynamic_cast<DataTableColumn<T>*>(this);

            if( Column )
            {
                return (*Column)[RowNumber]; // implicit conversion from T to Result<T>
            }

            return Result<T>::fromError("Incorrect type.");
        }

        template <typename T>
        Result<bool> Set(const int& RowNumber, T In)
        {
            // Make sure the column is the right type
            DataTableColumn<T>* Column = dynamic_cast<DataTableColumn<T>*>(this);

            if( Column )
            {
                (*Column)[RowNumber] = In;
                return true; // implicit conversion from bool to Result<bool>
            }

            return Result<bool>::fromError("Incorrect type.");
        }

    private:

        std::string _name;
};

#endif // DATATABLECOLUMNBASE_H
