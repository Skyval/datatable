#ifndef DATATABLEROW_H
#define DATATABLEROW_H

#include <string>

#include "DataTableColumnBase.h"
#include "Result.h"

class DataTable;
//class DataTableColumnBase;
template<typename T> class DataTableColumn;


class DataTableRow
{
    public:
        DataTableRow(DataTable* table);
        virtual ~DataTableRow();

        DataTableColumnBase* GetColumn(std::string name) const;

        void SetRowNumber(int number);

        std::string ToString();

        template<typename T>
        //bool Set(std::string name, T value)
        Result<bool> Set(std::string name, T value)
        {
            DataTableColumnBase* BaseColumn = GetColumn(name);

            if( BaseColumn != nullptr)
            {
                return BaseColumn->Set<T>(_rowNumber, value);
            }

            return Result<bool>::fromError("Could not find a column with that name.");//false;
        }

        template<typename T>
        Result<T> Get(std::string name) const
        {
            DataTableColumnBase* BaseColumn = GetColumn(name);

            if(BaseColumn == nullptr)
            {
                return Result<T>::fromError("Could not find a column with that name.");
            }

            return BaseColumn->Get<T>(_rowNumber);
        }

    private:
        DataTableRow() = delete; // Forced private default constructor.
        DataTableRow(const DataTableRow&) = delete; // Forced private copy constructor
        void operator=(const DataTableRow&) = delete; // Forced private copy assignment operator

        int _rowNumber;

        DataTable* _table;
};

#endif // DATATABLEROW_H
