#ifndef DATATYPE_H
#define DATATYPE_H


enum class EDataType
{
    CHAR,
    INT,
    FLOAT,
    STRING
};

#endif // DATATYPE_H
