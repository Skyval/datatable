#ifndef DATATABLEROWCOLLECTION_H
#define DATATABLEROWCOLLECTION_H

#include <vector>
#include "DataTableRow.h"

class DataTableRowCollection
{
    public:

        typedef std::vector<DataTableRow*>::iterator iterator;
        typedef std::vector<DataTableRow*>::const_iterator const_iterator;

        iterator begin() { return _rows.begin(); }
        iterator end() { return _rows.end(); }

        const_iterator begin() const { return _rows.begin(); }
        const_iterator end() const { return _rows.end(); }

        void AddRow(DataTableRow* Row);

        ~DataTableRowCollection();

        int size();

    protected:

    private:

        std::vector<DataTableRow*> _rows;
};

#endif // DATATABLEROWCOLLECTION_H
