#ifndef DATATABLE_H
#define DATATABLE_H

#include "DataTableColumnCollection.h"
#include "DataTableRowCollection.h"
#include "DataTableRow.h"
#include "DataTableColumn.h"

class DataTable
{
    public:

        /** Iterators for columns and rows */
        typedef DataTableColumnCollection::iterator ColumnIterator;
        typedef DataTableColumnCollection::const_iterator const_ColumenInterator;

        typedef DataTableRowCollection::iterator RowIterator;
        typedef DataTableRowCollection::const_iterator const_RowIterator;

        ColumnIterator ColumnBegin() { return _columnCollection.begin(); }
        const_ColumenInterator ColumnBegin() const { return _columnCollection.begin(); }

        ColumnIterator ColumnEnd() { return _columnCollection.end(); }
        const_ColumenInterator ColumnEnd() const { return _columnCollection.end(); }

        RowIterator RowBegin() { return _rowCollection.begin(); }
        const_RowIterator RowBegin() const { return _rowCollection.begin(); }

        RowIterator RowEnd() { return _rowCollection.end(); }
        const_RowIterator RowEnd() const { return _rowCollection.end(); }

        /** Adds a column to the DataTable. NewColumn does this automatically. */
        void AddColumn(DataTableColumnBase* Column);

        /** Prints the names of all the columns, in order */
        void PrintColumnNames();

        /**
         * Creates a new row. Rows should only be created this way.
         *
         * @return  The row, for direct access.
         */
        DataTableRow* NewRow();

        /**
         * Gets a column using its name.
         * @param name  The name of the column
         * @return  The column.
         */
        DataTableColumnBase* GetColumn(std::string name);

        /** Prints the contents of the table. */
        void PrintTable();

        /**
         * Creates a new column of type T
         *
         * @tparam T    The type of the column to create
         * @param name  The name to assign to the column
         * @return  The column created, if direct immediate access if necessary.
         */
        template<typename T>
        DataTableColumn<T>* NewColumn(std::string name)
        {
            DataTableColumn<T>* Column = new DataTableColumn<T>(name);
            _columnCollection.AddColumn(Column);
            return Column;
        }

    private:

        DataTableColumnCollection _columnCollection;

        DataTableRowCollection _rowCollection;
};

#endif // DATATABLE_H
